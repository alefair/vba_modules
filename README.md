# vba_modules

Modules for VBA projects

## List Modules

- SAPAPI



## Descriptions

### [SAPAPI]

- **Class_Initialize()**
    <br><code>set gui = new SAPAPI</code>

- **Connect(login, pswd, basename)**
    <br><code>set connection = gui.Connect('user', '*************', 'base_name')</code>

- **CloseConnection(Optional conn = Nothing, Optional id = "", Optional child = -1, Optional all = False)**
    <br><code>gui.CloseConnection(all:=True)</code>

- **CloseSession(Optional conn = Nothing, Optional sess = Nothing, Optional id = "", Optional child = -1, Optional all = False)**
    <br><code>gui.CloseSession(all:=True)</code>

- **CreateSession(Optional conn = Nothing)**
    <br><code>set session = gui.CreateSession(connection)</code>

- **GetConnection(Optional conn = 0)**
    <br><code>set connection2 = gui.GetConnection(1)</code>

- **GetSession(Optional conn = Nothing, Optional sess = 0)**
    <br><code>set session2 = gui.GetSession(connection, 1)</code>

- **GetConnections()**
    <br><code>set connections = GetConnections()</code>
    ```
        {
            '/app/con[0]': <COMObject <unknown>>,
            '/app/con[1]': <COMObject <unknown>>,
            '/app/con[2]': <COMObject <unknown>>
        }
    ```

- **GetSessions()**
    <br><code>set sessions = GetSessions()</code>
    ```
        {
            '/app/con[0]/ses[0]': <COMObject <unknown>>,
            '/app/con[0]/ses[1]': <COMObject <unknown>>,
            '/app/con[0]/ses[2]': <COMObject <unknown>>,
            '/app/con[2]/ses[1]': <COMObject <unknown>>
        }
    ```

Example

```
Dim gui as SAPAPI

set gui = new SAPAPI



Dim connect, session



'#open Sap session

set connect = gui.Connect(user, pswd, "SAP Prod db")

set session = gui.GetSession(connect)



'#sap script from sap recorder

session.findById("wnd[0]/tbar[0]/okcd").text = "SE16N"

session.findById("wnd[0]").sendVKey 0



session.findById("wnd[0]/usr/ctxtGD-TAB").text = "BUT000"

session.findById("wnd[0]").sendVKey 0



'#Close Connection

gui.CloseConnection(all:=True)
```
