VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "SAPAPI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'name:   SAPAPI Class
'author: @alefair
'

#If Win64 = 1 And VBA7 = 1 Then
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal Milliseconds As LongPtr)
#ElseIf Win32 = 1 Then
    Private Declare Sub Sleep Lib "kernel32" (ByVal Milliseconds As LongPtr)
#End If


Private session, connection, application, SapGuiAuto
Private SAPGUIPATH, pid
Private connections As Dictionary, sessions As Dictionary
Private login, pswd, basename
Private error_data, sap_title

Enum log_type
    INFO = 0
    warning = 1
    CRITICAL = 2
    Error = 3
End Enum

Enum status_type
    vbNew = 0
    vbInProgress = 1
    vbSuccess = 2
    vbFailed = 3
End Enum


Private Sub log(Optional logtype As Integer = 0, Optional msg As String = "")
    Dim msg_type_msg As String
    
    Select Case logtype
        Case Is = log_type.INFO
            msg_type_msg = "INFO"
        Case Is = log_type.warning
            msg_type_msg = "WARN"
        Case Is = log_type.CRITICAL
            msg_type_msg = "CRIT"
        Case Is = log_type.Error
            msg_type_msg = "ER-R"
    End Select
    
    Debug.Print Now() & ";" & "[" & msg_type_msg & "]" & ";" & msg & vbCrLf
    
End Sub
Private Function get_process_pid(name) As Dictionary
    Dim processlist As Dictionary, objWMIService, colItems, objitem, target_process_name, pid, username, Return_number
    
    Set processlist = New Dictionary
    
    Sleep (300)
    Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
    Sleep (300)
    Set colItems = objWMIService.ExecQuery("Select * From Win32_Process where name Like '" & name & "%'")

    For Each objitem In colItems
        target_process_name = objitem.name
        pid = objitem.ProcessID
        
        If UCase(target_process_name) Like "*" & UCase(name) & "*" Then
            Return_number = objitem.GetOwner(username)
            If Return_number = 0 Then
                If UCase(username) = UCase(Environ$("UserName")) Then
                    processlist.Add pid, target_process_name
                End If
            End If
        End If
        
    Next
    
    Set objWMIService = Nothing
    Set colItems = Nothing
    Set objitem = Nothing
    
    Set get_process_pid = processlist
End Function

Private Sub Error_msg(msg, Optional source)
    log log_type.Error, "Error: " & msg & IIf(source <> "", " in " & source, "")
    MsgBox "Error: " & msg & IIf(source <> "", " in " & source, ""), vbCritical, "������!"
End Sub

Private Sub Class_Initialize()

    '������ �������������:
    
    'Dim gui As SAPAPI
    'Set gui = New SAPAPI

    '������ ��������� ���������� � �������
    
    'gui.application        - ������ ��������� ���������� SAP
    'gui.connection         - ��������� �������� ����������
    'gui.session            - ��������� �������� ������
    
    'gui.SAPGUIPATH         - ���� � Saplogon.exe

    'gui.pid                - pid ����������� ���������� SAP
    

    'gui.Connect()          - ������� ��������� SAP
    
    'gui.CloseConnection()  - ��������� ����������
    'gui.CloseSession()     - ��������� ������
    
    
    'gui.Kill()             - ������� SAP
    'gui.Reset()            - ������� �������� ��-���������
    
    'gui.CreateSession()    - ������� ����� ������
    
    'gui.GetConnection()    - ������ ��������� ������������ ����������
    'gui.GetSession()       - ������ ��������� ������ ����������
    
    'gui.GetConnections()   - ������ ������ �������� ����������
    'gui.GetSessions()      - ������ ������ �������� ������
    
    
    'Set session = Nothing
    'Set connection = Nothing
    'Set application = Nothing
    'Set SapGuiAuto = Nothing
    
    SAPGUIPATH = "C:\Program Files (x86)\SAP\FrontEnd\SAPgui\saplogon.exe"
    pid = -1
    
    Set connections = New Dictionary
    Set sessions = New Dictionary
    
    login = ""
    pswd = ""
    basename = ""
    
    error_data = ""
    sap_title = ""
    
    
    Err.Clear
    
    On Error Resume Next
    Set SapGuiAuto = GetObject("SAPGUI")
    
    Dim pid_by_name As Dictionary
    
    If Not IsObject(SapGuiAuto) Then
        log log_type.warning, "��� �� �������. ���������..."
        
        Shell (SAPGUIPATH)
        Sleep 1000
        
        Err.Clear
        On Error Resume Next
        Set SapGuiAuto = GetObject("SAPGUI")
        
        Set pid_by_name = get_process_pid("saplogon")
        
        If Err.Number <> 0 Then Err.Raise vbObjectError + 4412, "SAPAPI", "������ ������� SAP!"
    Else
        Set pid_by_name = get_process_pid("saplogon")
    End If
    
    pid = pid_by_name.Keys(0)
    
    Set application = SapGuiAuto.GetScriptingEngine
    
    Sleep 1000
    
    If Not IsObject(application) Then Err.Raise vbObjectError + 4412, "SAPAPI", "�������� ����������� � GUI"
    
    GetConnections
End Sub

Public Function Connect(login, pswd, basename)
    
    '����������: ������� ���� SAP
    
    'login    SAP �����                  default :: ��������, user
    'pswd     ������ �� SAP ������       default :: ***********
    'basename ��� SAP �������            default :: base_name
    
    '��������:
    '������� ����� ���� � SAP � ����������� ������� basename � �������� ������� login � pswd
    
    '������ �������������:
    'gui.Connect('user', '*************', 'base_name')
    
    
    log log_type.warning, "SAP Connect starting with login as " & login & " in the system " & basename
    
    Err.Clear
    
    On Error Resume Next
    
    log log_type.INFO, "������� ������� �����������..."
    Set connection = application.OpenConnection(basename, True)
    
    Sleep 1200
    
    If Not IsObject(connection) Then
        Error_msg Err.Description, Err.source
    Else
        connections.Add connection.id, connection
    End If
    
    Sleep 1200
    
    Set session = GetSession(connection, 0)
    
    log log_type.INFO, "�����������..."
    
    Err.Clear
    
    On Error Resume Next
    
    '������ ����� � ������
    session.findById("wnd[0]/usr/txtRSYST-BNAME").Text = login
    session.findById("wnd[0]/usr/pwdRSYST-BCODE").Text = pswd
    session.findById("wnd[0]").sendVKey (0)
    
    Sleep 1000
    
    If UCase(session.findById("wnd[0]/sbar").Text) Like UCase("*����������*") Then
        CloseConnection connection
        Error_msg session.findById("wnd[0]/sbar").Text, "SAPAPI -> Connect"
        Exit Function
    Else
        Err.Clear
        On Error Resume Next
        
        If UCase(session.findById("wnd[1]").Text) Like UCase("*��������� ���������*") Then
            session.findById("wnd[1]/tbar[0]/btn[0]").press
            log log_type.warning, session.findById("wnd[1]/usr/lbl[1,5]").Text
        End If
        
        If UCase(session.findById("wnd[1]/usr/txtMESSTXT1").Text) Like UCase("*�����*") Then
            session.findById("wnd[1]/tbar[0]/btn[0]").press
            log log_type.warning, session.findById("wnd[1]/usr/txtMESSTXT1").Text
        End If
    End If
    
    log log_type.INFO, session.findById("wnd[0]/titl").Text
    
    log log_type.warning, "connection id is " & connection.id
    
    sessions.Add session.id, session
    
    Set Connect = connection
End Function


Public Function CloseConnection(Optional conn = Nothing, Optional id = "", Optional child = -1, Optional all = False)

    '����������: ��������� ����������
            
    '� �������� ���������� ���������� ����� ����:
    
    '- conn     SAP ����������                 default   :: connection
    '- id       ������������� ����������       ��������  :: /app/con[0]
    '- child    ���������� ����� ����������    ��������  :: 0, ����� ����������� ���: /app/con[**child**]
    
    '��������:
    '��������� ���������� SAP
    
    '������ �������������:
    
    'child_conn = 2
    'id_conn    = '/app/con[2]'
    
    'connection = None
    'connection = gui.GetConnections()[id_conn]
    'connection = gui.connection
    'connection = gui.GetConnection(child_conn)
    
    'gui.CloseConnection()                  - ��������� ��������� ��������� ���������� ������� gui.Connect()
    'gui.CloseConnection(connection)        - ��������� ���������� ���������� connection
    'gui.CloseConnection(id=id_conn)        - ��������� ���������� �� ��� ��������������
    'gui.CloseConnection(child=child_conn)  - ��������� ���������� �� ������ ����������
    'gui.CloseConnection(all=True)          - ��������� ��� ����������
    
    Dim target_conn_id, target_conn, con_id
    
    Err.Clear
    On Error Resume Next
    
    If all = True Then
        For Each target_conn_id In GetConnections:
            Set target_conn = application.findById(target_conn_id)
            con_id = target_conn.id
            target_conn.CloseSession (0)
            target_conn.CloseConnection
            log log_type.warning, "Connection " + con_id + " was closed!"
        Next
        Exit Function
    End If
    
    If id <> "" Then
        target_conn = application.findById(id)
        con_id = target_conn.id
        target_conn.CloseSession (0)
        target_conn.CloseConnection
        log log_type.warning, "Connection " + con_id + " was closed!"
        Exit Function
    End If
    
    If child <> -1 Then
        Set target_conn = application.findById("/app/con[" & child & "]")
        con_id = target_conn.id
        target_conn.CloseSession (0)
        target_conn.CloseConnection
        log log_type.warning, "Connection " + con_id + " was closed!"
        Exit Function
    End If
    
    If Not conn Is Nothing Then
        Set target_conn = conn
    Else
        Set target_conn = connection
    End If
    
    con_id = target_conn.id
    target_conn.CloseSession (0)
    target_conn.CloseConnection
    log log_type.warning, "Connection " + con_id + " was closed!"
    
    If Err.Number <> 0 Then
        Error_msg Err.Description, Err.source
        Exit Function
    End If
End Function


Public Function CloseSession(Optional conn = Nothing, Optional sess = Nothing, Optional id = "", Optional child = -1, Optional all = False)

    '����������:     ��������� ������
    
    '� �������� ���������� ������ ����� ����:
    
    '- conn     SAP ����������                 default   :: connection
    '- sess     SAP ������                     default   :: session
    
    '- id       ������������� ������           ��������  :: /app/con[0]/ses[0]
    '- child    ���������� ����� ������        ��������  :: 0, ����� ����������� ���: connection.FindById('ses[**child**]')
    '- all      ��������� ��� ������           default   :: False
    
    '��������:
    '��������� ������ SAP
    
    '������ �������������:
    
    'child_conn = 2
    'id_conn = '/app/con[2]'
    
    'connection = None
    'connection = gui.GetConnections()[id_conn]
    'connection = gui.connection
    'connection = gui.GetConnection(child_conn)
    
    'child_sess = 1
    'id_sess    = '/app/con[2]/ses[1]'
    
    'session = None
    'session    = gui.GetSessions()[id_sess]
    'session = gui.session
    'session = gui.GetSession(child_sess)
    
    
    'gui.CloseSession()                        - ��������� ��������� �������� ������
    'gui.CloseSession(connection)              - ��������� gui.session �� ���������� connection
    'gui.CloseSession(connection, session)     - ��������� ������ session � ���������� connection
    
    'gui.CloseSession(id=id_sess)              - ��������� ������ �� �� ��������������
    
    'gui.CloseSession(connection, child_sess)  - ��������� ������ �� �� ������ � ���������� connection
    'gui.CloseSession(connection, all=True)    - ��������� ��� ������ � ���������� connection
    
    
    'gui.CloseSession(all=True)                - ��������� ��� ������ ���� ����������
    
    Dim target_sess_id, target_sess
    
    Err.Clear
    On Error Resume Next
    
    If all = True Then
        For Each target_sess_id In GetSessions:
            Set target_sess = application.findById(target_sess_id)
            If Not conn Is Nothing Then
                If conn.id = target_sess.Parent.id Then
                    sess_id = target_sess.id
                    target_sess.Parent.CloseSession (sess_id)
                    log log_type.warning, "Session " & sess_id & " was closed!"
                End If
            Else:
                sess_id = target_sess.id
                target_sess.Parent.CloseSession (sess_id)
                log log_type.warning, "Session " & sess_id & " was closed!"
            End If
        Next
        Exit Function
    End If
    
    If id <> "" Then
        Set target_sess = application.findById(id)
        sess_id = target_sess.id
        target_sess.Parent.CloseSession (sess_id)
        log log_type.warning, "Session " & sess_id & " was closed!"
        Exit Function
    End If
    
    If child <> -1 Then
        If conn Is Nothing Then
            Error_msg "����������� �� ����� ���� ������!", "SAPAPI -> CloseSession"
            Exit Function
        End If
        
        Set target_sess = conn.findById("ses[" & child & "]")
        sess_id = target_sess.id
        target_sess.Parent.CloseSession (sess_id)
        log log_type.warning, "Session " & sess_id & " was closed!"
        Exit Function
    End If
    
    If Not conn Is Nothing And Not sess Is Nothing Then
        Set target_sess = sess
        sess_id = target_sess.id
        conn.CloseSession (sess_id)
        log log_type.warning, "Session " & sess_id & " was closed!"
        Exit Function
    End If
    
    If conn Is Nothing And Not sess Is Nothing Then
        If Not IsObject(connection) Then
            Error_msg "�� ���������� �������� ����������!", "SAPAPI -> CloseSession"
            Exit Function
        End If
        
        Set target_sess = sess
        sess_id = target_sess.id
        connection.CloseSession (sess_id)
        log log_type.warning, "Session " & sess_id & " was closed!"
        Exit Function
     End If
     
    If conn Is Nothing And sess Is Nothing Then
        If Not IsObject(connection) Then
            Error_msg "�� ���������� �������� ����������!", "SAPAPI -> CloseSession"
            Exit Function
        End If
        
        If Not IsObject(session) Then
            Error_msg "�� ���������� �������� ������!", "SAPAPI -> CloseSession"
            Exit Function
        End If
        
        Set target_sess = session
        sess_id = target_sess.id
        connection.CloseSession (sess_id)
        log log_type.warning, "Session " & sess_id & " was closed!"
        Exit Function
    End If
    
    If Not conn Is Nothing And sess Is Nothing Then
        If Not IsObject(session) Then
            Error_msg "�� ���������� �������� ������!", "SAPAPI -> CloseSession"
            Exit Function
        End If
            
        Set target_sess = session
        sess_id = target_sess.id
        CloseSession (sess_id)
        log log_type.warning, "Session " & sess_id & " was closed!"
        Exit Function
    End If
    
    If Err.Number <> 0 Then
        Error_msg Err.Description, Err.source
    End If
End Function



Public Function CreateSession(Optional conn = Nothing)
    
    '������� ����� ������ �� ���������� conn
        
    '������� �������������
    
    'child_conn = 2
    'id_conn = '/app/con[2]'
    
    'connection = None
    'connection = gui.GetConnections()[id_conn]
    'connection = gui.connection
    'connection = gui.GetConnection(child_conn)
    
    'gui.CreateSession (connection)
    
    Dim sess_list As Dictionary, new_list As Dictionary
    
    Err.Clear
    On Error Resume Next
    
    Set sess_list = GetSessions
            
    If Not conn Is Nothing Then
        Set session = conn.Children(0)
    Else
        If IsObject(connection) Then
            Set session = connection.Children(0)
        End If
    End If
           
    If Not IsObject(session) Then
        Error_msg "�� ���������� �������� ����������!", "SAPAPI -> CloseSession"
        Exit Function
    Else
        session.CreateSession
        Sleep 1200
    End If
    
    Set new_list = GetSessions
    
    
    '������� ������
    For Each item In sess_list.Keys
        new_list.Remove item
    Next
    

    If new_list.Count > 0 Then
        log log_type.warning, "������� ������ " & new_list.Keys(0)
        Set CreateSession = application.findById(new_list.Keys(0))
    End If
    
    If Err.Number <> 0 Then
        Error_msg Err.Description, Err.source
    End If
End Function


Public Function GetConnection(Optional conn = 0)
    
    '������ ��������� ���������� �� ��� ������
    
    '������ �������������:
    
    'conn = 0
    'connection = gui.GetConnection(conn)
    
    Dim temp_connection
    
    Err.Clear
    On Error Resume Next
    
    Set temp_connection = application.findById("/app/con[" & conn & "]")
    If Not IsObject(temp_connection) Then
        Exit Function
    Else:
        Set GetConnection = temp_connection
    End If
    
    If Err.Number <> 0 Then
        Error_msg Err.Description, Err.source
    End If
End Function


Public Function GetSession(Optional conn = Nothing, Optional sess = 0)
    
    '������ ���������� �� ��� ������ � ���������� connection
        
    '������ �������������:
    
    'child_conn = 2
    'id_conn = '/app/con[2]'
    
    'connection = None
    'connection = gui.GetConnections()[id_conn]
    'connection = gui.connection
    'connection = gui.GetConnection(child_conn)
    
    'child_sess = 1
    
    'session = gui.GetSession(connection, child_sess)
    
    Dim temp_session, temp_connection
    
    Err.Clear
    On Error Resume Next
    
    If conn Is Nothing Then
        Set temp_connection = connection
    Else
        Set temp_connection = conn
    End If
    
    Set temp_session = temp_connection.findById("ses[" & sess & "]")
    
    If Not IsObject(temp_session) Then
        Exit Function
    Else
        Set GetSession = temp_session
        Exit Function
    End If
    
    If Err.Number <> 0 Then
        Error_msg Err.Description, Err.source
    End If
End Function


'#####
'LISTS

Public Function GetConnections()
    
    '������ ������ ���� ����������:
    
    '{
        '/app/con[0]': <COMObject <unknown>>,
        '/app/con[1]': <COMObject <unknown>>,
        '/app/con[2]': <COMObject <unknown>>
    '}
    
    
    '������ �������������:
    
    'id_conn    = '/app/con[1]'
    
    'list_conn = gui.GetConnections()
    
    'connection    = gui.GetConnections()[id_conn]
    

    Set connections = New Dictionary
    
    For Each child In application.connections
        connections.Add child.id, child
    Next
    
    Set GetConnections = connections
End Function


Public Function GetSessions()
    
    '������ ������ ���� ����������:
    
    '{
        '/app/con[0]/ses[0]': <COMObject <unknown>>,
        '/app/con[0]/ses[1]': <COMObject <unknown>>,
        '/app/con[0]/ses[2]': <COMObject <unknown>>,
        '/app/con[2]/ses[1]': <COMObject <unknown>>
    '}
    
    '������ �������������:
    
    'id_sess    = '/app/con[0]/ses[2]'
    
    'list_sess = gui.GetSessions()
    
    'session    = gui.GetSessions()[id_sess]
    
    Set sessions = New Dictionary
    
    For Each child In application.connections
        For Each sess In child.sessions
            sessions.Add sess.id, sess
        Next
    Next
    Set GetSessions = sessions
End Function
